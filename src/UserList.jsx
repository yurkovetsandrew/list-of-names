import React from 'react';
import UserInfo from './UserInfo';
import UserForm from './UserForm';
import UserEdding from './UserEdding';
import UserPage from './UserPage';
import Analitics from './Analitics';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  Link,
  useRouteMatch
} from "react-router-dom";

class UserList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      darkTheme: false,
      users: [
        { name: 'Andrew', admin: false, highlighted: false, url: 'andrew' },
        { name: 'Michael', admin: false, highlighted: false, url: 'michael' },
        { name: 'Vasilii', admin: false, highlighted: false, url: 'vasilii' }
      ],
      filterValue: ''
    };
  }

  filterUser = (e) => this.setState({filterValue: e.target.value});

  deleteUser = (name) =>{
    const arr = [...this.state.users]
    let newArr = arr.filter(user => user.name !== name)
    this.setState({users: newArr, value: ''})
  }
  deleteHighlightedUsers = () =>{
    const arr = [...this.state.users]
    let newArr = arr.filter(user => user.highlighted == false)
    this.setState({users: newArr, value: ''})
  }

  highlightedUser = (name) =>{
    const users = [...this.state.users]
    let newUsers = users.map(user => {
      if(user.name === name){
        user.highlighted = !user.highlighted
        return  user
      }
      else {
        return user
      }
    })
    this.setState({users: newUsers, value: ''})
  }

  changeTheme = () =>{
    let newDarkTheme = !this.state.darkTheme;
    this.setState({darkTheme: newDarkTheme});
  }

  giveAdmin = (admin) =>{
    const users = [...this.state.users]
    let newUsers = users.map(user => {
      if(user.name === admin){
        user.admin = !user.admin
        return  user
      }
      else {
        return user
      }
    })
    this.setState({users: newUsers, value: ''})
  }

  updateUser = (oldName, newName) =>{
    const users = [...this.state.users]
    let newUsers = users.map(user => {
      if(user.name === oldName){
        user.name = newName
        return user
      }
      else {
        return user
      }
    })
    this.setState({users: newUsers, value: ''})
  }

  addUser = (name) => {
    if(name === ''){
      return
    }
    const newUsers = [...this.state.users]

    newUsers.push({name: name, admin: false , highlighted: false, url: name.toLowerCase()})

    this.setState({users: newUsers})
  }


  render(){
    const arr = [...this.state.users]
    let filtredUsers
    if(this.state.filterValue == '') {
      filtredUsers = arr
    }
    else {
      filtredUsers = arr.filter(user => user.name.toLowerCase().includes(this.state.filterValue.toLowerCase()))
    }

    const list = filtredUsers.map((user, index) =>
      <div key={index}>
        <UserInfo user={user} onDeleteUser={this.deleteUser}
                              onUpdateUser={this.updateUser}
                              onGiveAdmin={this.giveAdmin}
                              onHighlightedUser={this.highlightedUser} />
      </div>
    )
    let highlighted = 0
    this.state.users.map(user => user.highlighted && highlighted++);

    return (
      <div>
        <Router>
          <div className={ this.state.darkTheme ? 'main darkTheme' : 'main' }>
            <div className='cap'>
              <NavLink to="/">
                <a className='buttonOnCap home'>
                  <span className='buttonLine buttonLineTop'></span>
                  <span className='buttonLine buttonLineBottom'></span>
                  <span className='buttonLine buttonLineLeft'></span>
                  <span className='buttonLine buttonLineRight'></span>
                  List
                </a>
              </NavLink>
              <NavLink to="/analitics" activeClassName="selected">
                <button className='buttonOnCap'>Analytics</button>
              </NavLink>
              <button className='buttonOnCap dark' onClick={this.changeTheme}>Theme</button>
            </div>
                <div className='line'>
                </div>
              <Switch>
                <Route className='analytics' exact path="/">
                  <div>
                      <p className='zagolovok'>List of users:</p>
                      <p>Search user:</p>
                      <input className="filter" placeholder="Name" type="text" value={this.state.filterValue} onChange={this.filterUser} />
                    {this.state.users.length
                      ?<div>
                          {list}
                       </div>
                      :<p className='usersMissing'>Users are missing</p>
                    }
                    <UserForm onAddUser={this.addUser}/>
                    {highlighted?
                      <button className='delete2 send' onClick={this.deleteHighlightedUsers}>Remove selected users</button>:<p></p>}
                  </div>
                </Route>
                <Route path="/users/:name">
                  <UserPage onUpdateUser={this.updateUser} users={this.state.users}
                 onDeleteUser={this.deleteUser}/>
                </Route>
                <Route path="/analitics">
                  <Analitics users={this.state.users}/>
                </Route>
              </Switch>
          </div>
        </Router>
      </div>
    )
  }
}

export default UserList;
