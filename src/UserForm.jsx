import React from 'react';

class UserForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value || ''
    };
  }

  handleChange = (e) => {
    this.setState({value: e.target.value});
  }

  handleClick = () => {
    this.props.onAddUser(this.state.value)
    this.setState({value: ''})
  }

  render(){


    return (

      <div>
        <label>Add user name: </label>
        <input placeholder="New name" type="text" value={this.state.value} onChange={this.handleChange} />
        <input className="send" type="submit" value="Отправить" onClick={this.handleClick} />

      </div>
    )
  }
}
export default UserForm;
