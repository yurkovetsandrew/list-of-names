import React from 'react';
import UserEdding from './UserEdding';
import {
  Link,
  withRouter,
  Redirect
} from "react-router-dom";

class UserPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editing: false,
      deleting: false
    };
  }

  editUser = () => {
    this.setState({editing: true})
  }

  finishEditUser = () => {
    this.setState({editing: false})
  }

  deleteUser = (name) => {
    let sureDelete = window.confirm("Are you sure you want to permanently delete the user?")
    if(sureDelete){
      this.setState({deleting: true})
      this.props.onDeleteUser(name)
    }
  }

  render() {

    const url = this.props.match.params.name;
    const users = [...this.props.users]
    let user = users.find(user => {
      if(user.url == url){
        return true
      }
    })

    return(
      <div>
          {this.state.deleting ? (
            <div>
              <Redirect to="/" />
            </div>
           ):(
           <div>
            <img className='ava' src="/176-1760995_png-file-svg-user-icon-free-copyright-transparent.png" alt="Ошибка загрузки" />
            <p className='userName'>{user.name}</p>
            <button className='delete' onClick={ () => { this.deleteUser(user.name) } }>Delete this user</button>
              {
                this.state.editing ? (
                  <div>
                    <UserEdding value={user.name} onUpdateUser={this.props.onUpdateUser}
                                             onFinishEditUser={this.finishEditUser}/>
                  </div>
                 ) : (
            <div>
              <button className='edit1' onClick={this.editUser}>Edit name</button>
            </div>
             )}
          </div>
        )}
      </div>
    )
  }
}
export default withRouter(UserPage);
