import React from 'react';

class Analitics extends React.Component {
  constructor(props) {
    super(props);
  };

  render() {
    let onlineUsers = this.props.users.length
    let adminUsers = 0;
    this.props.users.map(user => user.admin && adminUsers++)

    return(
      <div>
       <p className='analitics'>{onlineUsers !=1? <p>Now we have {onlineUsers} users.</p>:<p>Now we have {onlineUsers} user.</p>}</p>
       <p className='analitics'>{adminUsers !=1? <p>{adminUsers} admins.</p>:<p>{adminUsers} admin.</p>}</p>
      </div>
    )
  }
}
export default Analitics;
