import React from 'react';
import UserForm from './UserForm';

class UserEdding extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value || ''
    };
  }

  handleChange = (e) => {
    this.setState({value: e.target.value});
  }

  handleClick = () => {
    this.props.onUpdateUser(this.props.value, this.state.value)
    this.setState({value: ''})

    this.props.onFinishEditUser()
  }

  render(){

    return (
      <div>
        <label>Edd user name: </label>
        <input type="text" value={this.state.value} onChange={this.handleChange} />
        <input className="send" type="submit" value="Обновить" onClick={this.handleClick} />
      </div>
    )
  }
}
export default UserEdding;
