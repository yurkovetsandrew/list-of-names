import React from 'react';
import UserEdding from './UserEdding';
import {
  Link
} from "react-router-dom";

class UserInfo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      editing: false
    };
  }

  editUser(){
    this.setState({editing: true})
  }

  finishEditUser = () => {
    this.setState({editing: false})
  }

  render(){
    const user = this.props.user
    return (
      <div>
        {
          this.state.editing ? (
            <div>
              <UserEdding value={user.name} onUpdateUser={this.props.onUpdateUser}
                                            onFinishEditUser={this.finishEditUser}/>
            </div>
          ) : (
          <div className="row">
            <span className={ user.admin ? 'admin' : {} }><input className='checkbox' type="checkbox" onChange={() =>
                   this.props.onHighlightedUser(user.name)} checked={user.highlighted } />
                    <Link to={"/users/" + user.url}>
                      {user.name}
                      <span className='buttonLine buttonLineBottomName'></span>
                    </Link>
            </span>
            <button className="x" onClick={() => this.props.onDeleteUser(user.name)}> X </button>
            <button className="e" onClick={() => this.editUser()}> E </button>
            <input className="checkbox" type="checkbox" onChange={() => this.props.onGiveAdmin(user.name)}
                   checked={user.admin} />
          </div>
          )}
      </div>
    )
  }
}
export default UserInfo;
