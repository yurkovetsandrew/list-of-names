import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ListOfNames from './UserList.jsx';

ReactDOM.render(
  <React.StrictMode>
    <ListOfNames />
  </React.StrictMode>,
  document.querySelector('body')
);
